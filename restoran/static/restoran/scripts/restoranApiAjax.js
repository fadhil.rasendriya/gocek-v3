$(document).ready(function () {
    $.ajax({
        method: 'GET',
        data: {},
        url: '/restoran/getrestoranlist/',
        success: function (result) {
            let contentContainer = $('#contentBox');
            contentContainer.empty();
            let length = result['total_items'];
            for (let i = 0; i < length; i++) {
                let item = result['items'][i];
                let nama = item['nama'];
                let alamat = item['alamat'];
                let picture = item['picture'];
                let timeOpen = item['time_open'];
                let timeClose = item['time_close'];
                let url = item['url'];
                contentContainer.append(
                    `<div class="card text-dark align-items-center">
                    <div class="card-body cardSelector">
                        <!--foto restoran -->
                        <img class="card-img-top" src="http://2.bp.blogspot.com/-n4XVTOEePrA/UsNPki9OppI/AAAAAAAAAD8/vvOve1Lqk3E/s1600/5-Restoran-cepat-saji-ist.jpg"
                             alt="Card image">
                        <div class="card-title"><strong>${nama}</strong></div>
                        <h6>
                            <strong>Alamat:</strong>
                        </h6>
                        <div class="card-title">${alamat}</div>
                        <h6>
                            <strong>Jam Operasional:</strong>
                        </h6>
                        <div class="card-title">${timeOpen} - ${timeClose}</div>

                        <div>
                            <a class="btn btn-primary btn-block ml-auto"
                               style="font-size: large; color: white; width: 12rem; margin-bottom: 30px ;margin-top: 30px;"
                               href="${url}"
                               role="button">Lihat Detail</a>
                        </div>
                    </div>
                </div>`
                );
            }
        },
        complete: function () {
            $('.cardSelector').hover(
                function () {
                    $(this).parent().addClass("hoverColor");
                }, function () {
                    $(this).parent().removeClass("hoverColor");
                });
        }
    });


});