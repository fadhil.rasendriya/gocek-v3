from django.contrib import admin
from .models import Restoran, Makanan
# Register your models here.

admin.site.register(Restoran)
admin.site.register(Makanan)
