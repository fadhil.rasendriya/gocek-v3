from django.urls import path
from . import views

app_name = 'restoran'

urlpatterns = [
    path('', views.index, name='index'),
    path('add/', views.add, name='add'),
    path('add-food/<str:id>', views.add_food, name='add_food'),
    path('detail/<str:id>', views.detail, name='detail'),
    path('update/<str:id>', views.update, name='update'),
    path('getrestoranlist/', views.restoran_api, name='restoran_api'),
    path('foodapi/', views.food_api, name='food_api'),
]
