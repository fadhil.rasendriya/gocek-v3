from django.test import TestCase, Client
from .models import Restoran, Makanan
from .forms import RestoranForm, MakananForm
from . import views
from django.urls.base import resolve
from django.core.files.uploadedfile import SimpleUploadedFile
from django.shortcuts import get_object_or_404


import tempfile
import datetime

class RestoranTest(TestCase):
    def dummy_restoran(self):
        test = 'Anonymous'
        test_time_open = datetime.time(0, 0, 0)
        test_time_close = datetime.time(1, 0, 0)
        image = tempfile.NamedTemporaryFile(suffix='.jpg').name
        obj = Restoran.objects.create(nama=test, alamat=test, time_open=test_time_open, time_close=test_time_close, picture=image)
        return obj

    def dummy_makanan(self):
        test = 'IniMakanan'
        price = 1
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        restoran = self.dummy_restoran()
        obj = Makanan.objects.create(nama=test, harga=price, restoran=restoran, picture=image)
        return obj

    def test_model_restoran_can_create(self):
        test = 'Anonymous'
        test_time_open = datetime.time(0, 0, 0)
        test_time_close = datetime.time(1, 0, 0)
        image = tempfile.NamedTemporaryFile(suffix='.jpg').name
        Restoran.objects.create(nama=test, alamat=test, time_open=test_time_close, time_close=test_time_close, picture=image)
        object_counter = Restoran.objects.all().count()
        self.assertEqual(object_counter, 1)

    def test_model_makanan_can_create(self):
        test = 'Anonymous'
        price = 1
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        restoran = Restoran.objects.create(nama=test, alamat=test, time_open=datetime.time(0,0,0), time_close=datetime.time(1,0,0), picture=image)
        Makanan.objects.create(nama=test, harga=price, restoran=restoran, picture=image)
        object_counter = Makanan.objects.all().count()
        self.assertEqual(object_counter, 1)

    def test_restoran_url_exists(self):
        response = Client().get('/restoran/')
        self.assertEqual(response.status_code, 200)

    def test_restoran_main_using_index_function(self):
        found = resolve('/restoran/')
        self.assertEqual(found.func, views.index)

    def test_add_restoran_url_exists(self):
        response = Client().get('/restoran/add/')
        self.assertNotEqual(response.status_code, 404)

    def test_add_restoran_using_add_function(self):
        found = resolve('/restoran/add/')
        self.assertEqual(found.func, views.add)

    def test_detail_url_exists(self):
        self.dummy_restoran()
        response = Client().get('/restoran/detail/1')
        self.assertEqual(response.status_code, 200)

    ## Commented out for becoming irrelevant on 01/01/2021
    '''
    def test_makanan_exists_in_detail(self):
        self.dummy_makanan()
        response = Client().get('/restoran/detail/1')
        html_response = response.content.decode('utf8')
        self.assertIn('IniMakanan', html_response)
    '''
    def test_restoran_exists(self):
        self.dummy_restoran()
        response = Client().get('/restoran/detail/1')
        html_response = response.content.decode('utf8')
        self.assertIn('Anonymous', html_response)

    def test_detail_using_detail_function(self):
        self.dummy_restoran()
        found = resolve('/restoran/detail/1')
        self.assertEqual(found.func, views.detail)

    def test_update_url_exists(self):
        self.dummy_restoran()
        response = Client().get('/restoran/update/1')
        self.assertNotEqual(response.status_code, 404)

    def test_update_using_update_function(self):
        self.dummy_restoran()
        found = resolve('/restoran/update/1')
        self.assertEqual(found.func, views.update)

    def test_add_makanan_using_add_food_function(self):
        self.dummy_restoran()
        found = resolve('/restoran/add-food/1')
        self.assertEqual(found.func, views.add_food)

    def registerDummy(self):
        data = {'username': 'toru', 'password1': 'notchill', 'password2': 'notchill', 'email': 'toru@tkmail.com'}
        self.client.post('/account/signup/', data=data)

    def test_add_restoran_needs_login(self):
        response = self.client.get('/restoran/add/', follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn('username', html_response)
        self.assertIn('login', html_response)

    def test_add_restoran_after_login(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        self.client.post('/account/login/', data=data)
        response = self.client.get('/restoran/add/')
        self.assertEqual(response.status_code, 200)

    def test_add_makanan_url_exists(self):
        self.dummy_restoran()
        response = self.client.get('/restoran/add-food/1')
        self.assertNotEqual(response.status_code, 404)

    def test_add_makanan_needs_login(self):
        self.dummy_restoran()
        response = self.client.get('/restoran/add-food/1', follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn('username', html_response)
        self.assertIn('login', html_response)

    def test_update_need_login(self):
        self.dummy_restoran()
        response = self.client.get('/restoran/update/1', follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn('username', html_response)
        self.assertIn('login', html_response)

    def test_add_restoran_api_fail(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        self.client.post('/account/login/', data=data)
        TORU = 'TORU'
        data = {'name': TORU, 'alamat': TORU}
        response = self.client.post('/restoran/add/', data=data)
        json_response = eval(response.content.decode('utf8'))
        self.assertIn('invalid', json_response['message'])

    def test_add_restoran_api_success(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        self.client.post('/account/login/', data=data)
        test = 'Toru'
        test_time_open = datetime.time(0, 0, 0)
        test_time_close = datetime.time(1, 0, 0)
        image = SimpleUploadedFile(name='test_image.jpg', content=open('images/angiospermae.jpg', 'rb').read(), content_type='image/jpeg')
        data = {'nama': test, 'alamat': test, 'time_open': test_time_open, 'time_close': test_time_close,
                                      'picture': image, 'kategori': test, 'jenis_pesanan': test, 'telepon': test}
        response = self.client.post('/restoran/add/', data=data)
        json_response = eval(response.content.decode('utf8'))
        self.assertIn('berhasil', json_response['message'])

    def test_add_makanan_success(self):
        self.dummy_restoran()
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        self.client.post('/account/login/', data=data)
        test = 'Madoka'
        harga = 1
        image = SimpleUploadedFile(name='test_image.jpg', content=open('images/angiospermae.jpg', 'rb').read(),
                                   content_type='image/jpeg')

        data = {'nama': test, 'harga': harga, 'picture': image, 'restoran_id': 1}
        response = self.client.post('/restoran/add-food/1', data=data, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_add_makanan_fail(self):
        self.dummy_restoran()
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        self.client.post('/account/login/', data=data)
        test = 'Madoka'
        harga = 1
        image = SimpleUploadedFile(name='test_image.jpg', content=open('images/angiospermae.jpg', 'rb').read(),
                                   content_type='image/jpeg')

        data = {'nama': test, 'picture': image, 'restoran_id': 1}
        response = self.client.post('/restoran/add-food/1', data=data, follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn('Invalid form', html_response)

    def test_update_restoran_success(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        self.client.post('/account/login/', data=data)
        self.dummy_restoran()
        test = 'Toru'
        test_time_open = datetime.time(0, 0, 0)
        test_time_close = datetime.time(1, 0, 0)
        image = SimpleUploadedFile(name='test_image.jpg', content=open('images/angiospermae.jpg', 'rb').read(),
                                   content_type='image/jpeg')
        data = {'time_open': test_time_open, 'time_close': test_time_close,
                'picture': image, 'kategori': test, 'telepon': test}
        response = self.client.post('/restoran/update/1', data=data, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_update_restoran_post_login(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        self.client.post('/account/login/', data=data)
        self.dummy_restoran()
        response = self.client.get('/restoran/update/1')
        self.assertEqual(response.status_code, 200)


    def test_get_restoran_api(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        self.client.post('/account/login/', data=data)
        self.dummy_restoran()
        response = self.client.get('/restoran/getrestoranlist/')
        json_response = eval(response.content.decode('utf8'))
        self.assertEqual(json_response['items'][0].get('nama'), 'Anonymous')


    def test_food_api(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        self.client.post('/account/login/', data=data)
        self.dummy_makanan()
        data = {'id': '1'}
        response = self.client.get('/restoran/foodapi/', data=data)
        json_response = eval(response.content.decode('utf8'))
        self.assertEqual(json_response['items'][0].get('nama'), 'IniMakanan')


