from django.shortcuts import render, HttpResponseRedirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from .forms import RestoranForm, MakananForm
from .models import Restoran, Makanan
from django.contrib import messages
from django.http import JsonResponse


def index(request):
    list_restoran = Restoran.objects.all()
    context = {'list_restoran': list_restoran}
    return render(request, 'restoran/index.html', context)

@login_required(login_url='/account/login/')
def add(request):

    if request.method == 'POST':
        data = {'message': ''}
        form = RestoranForm(request.POST, request.FILES)
        if form.is_valid():
            nama = form.cleaned_data.get('nama')
            alamat = form.cleaned_data.get('alamat')
            picture = form.cleaned_data.get('picture')
            kategori = form.cleaned_data.get('kategori')
            jenis_pesanan = form.cleaned_data.get('jenis_pesanan')
            time_open = form.cleaned_data.get('time_open')
            time_close = form.cleaned_data.get('time_close')
            telepon = form.cleaned_data.get('telepon')
            obj = Restoran.objects.create(nama=nama,telepon=telepon, alamat=alamat,picture=picture, jenis_pesanan=jenis_pesanan, time_open=time_open, time_close=time_close, kategori=kategori)
            obj.save()
            data['message'] = 'Restoran berhasil ditambahkan'
        else:
            data['message'] = 'invalid form'
        return JsonResponse(data)
    form = RestoranForm()
    context = {'form': form}
    return render(request, 'restoran/add.html', context)

@login_required(login_url='/account/login/')
def add_food(request, id):
    form = MakananForm()
    if request.method == 'POST':
        form = MakananForm(request.POST, request.FILES)
        if form.is_valid():
            nama = form.cleaned_data.get('nama')
            harga = form.cleaned_data.get('harga')
            picture = form.cleaned_data.get('picture')
            restoran_id = request.POST['restoran_id']
            restoran = get_object_or_404(Restoran, id=restoran_id)
            obj = Makanan.objects.create(nama=nama, harga=harga, picture=picture, restoran=restoran)
            obj.save()
            return HttpResponseRedirect('/restoran/detail/' + id)
        else:
            messages.error(request, 'Invalid form')
    context = {'form': form, 'restoran_id': id}
    return render(request, 'restoran/add_food.html', context)

def detail(request, id):
    restoran = get_object_or_404(Restoran, id=id)
    context = {'restoran': restoran}
    foods = Makanan.objects.all()
    food_exists = False
    if foods:
        context['foods'] = foods
        food_exists = True
    context['food_exists'] = food_exists
    return render(request, 'restoran/detail.html', context)

@login_required(login_url='/account/login/')
def update(request, id):
    restoran = Restoran.objects.get(id=id)
    form = RestoranForm()
    if request.method == 'POST':
        form = RestoranForm(request.POST, request.FILES)
        if form.is_valid() or True:
            picture = form.cleaned_data.get('picture')
            kategori = form.cleaned_data.get('kategori')
            time_open = form.cleaned_data.get('time_open')
            time_close = form.cleaned_data.get('time_close')
            telepon = form.cleaned_data.get('telepon')
            restoran.picture = picture
            restoran.kategori = kategori
            restoran.time_open = time_open
            restoran.time_close = time_close
            restoran.telepon = telepon
            restoran.save()
        return HttpResponseRedirect('/restoran/detail/' + id)
    context = {'form': form, 'restoran': restoran}

    return render(request, 'restoran/update.html', context)

def restoran_api(request):
    data = {'total_items': 0, 'items': []}
    restorans = Restoran.objects.all()
    for restoran in restorans:
        nama = restoran.nama
        alamat = restoran.alamat
        picture = 'http://2.bp.blogspot.com/-n4XVTOEePrA/UsNPki9OppI/AAAAAAAAAD8/vvOve1Lqk3E/s1600/5-Restoran-cepat-saji-ist.jpg'
        time_open = restoran.time_open.strftime("%H:%M")
        time_close = restoran.time_close.strftime("%H:%M")
        id = restoran.id
        url = f"/restoran/detail/{id}"
        data['items'].append({'nama': nama, 'alamat': alamat, 'picture': picture, 'time_open': time_open, 'time_close': time_close, 'url': url})
        data['total_items'] += 1
    return JsonResponse(data)

def food_api(request):
    data = {'total_items': 0, 'items': []}
    restoran_id = request.GET.get('id')
    restoran = get_object_or_404(Restoran, id=restoran_id)
    foods = Makanan.objects.all()
    for makanan in foods:
        if restoran == makanan.restoran:
            nama = makanan.nama
            picture = 'http://2.bp.blogspot.com/-n4XVTOEePrA/UsNPki9OppI/AAAAAAAAAD8/vvOve1Lqk3E/s1600/5-Restoran-cepat-saji-ist.jpg'
            harga = f"Rp{makanan.harga}"
            data['items'].append({'nama': nama, 'picture': picture, 'harga': harga})
            data['total_items'] += 1
    return JsonResponse(data)
