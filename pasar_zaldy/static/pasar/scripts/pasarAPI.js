$(document).ready(function () {
    $.ajax({
        method: 'GET',
        data: {},
        url: '/pasar/getpasarlist/',
        success: function (result) {
            let contentContainer = $('#contentBox');
            contentContainer.empty();
            let length = result['total_items'];
            for (let i = 0; i < length; i++) {
                let item = result['items'][i];
                let nama = item['namaPasar'];
                let alamat = item['alamatPasar'];
                let foto = item['fotoPasar'];
                let buka = item['jambukaPasar'];
                let tutup = item['jamtutupPasar'];
                let status = item['statuscovidPasar'];
                let url = item['url'];
                contentContainer.append(
                    `
                    <div class="card text-dark align-items-center lh-0"
            style="width: 20rem; box-shadow: 1px 5px 5px 0 rgba(0,0,0,0.2); margin-right: 50px; 
            margin-bottom: 50px; max-width: 100%; align-content: center; border-radius: 5px ;">

            <div class="card-body cards" >
                <img class="card-img-top" src="${foto}" alt="Foto Pasar" style="height: 220px; border-radius: 5px ">
                <div class="card-title" style="text-align: center;"><strong>${nama}</strong></div>

                <h6>
                    <strong>Alamat:</strong>
                </h6>
                <div class="card-title">${alamat}</div>

                <h6>
                    <strong>Jam Operasional:</strong>
                </h6>
                <div class="card-title">${buka} - ${tutup}</div>

                <h6>
                    <strong>Status Covid:</strong>
                </h6>
                <div class="card-title">${status}</div>

                <div class="text-center"><a id="detail" href="${url}" href="#"
                        class="btn btn-primary" style="border-radius: 20px; width: 8rem;">Lihat Detail</a></div>
            </div>
                `
                );
            }
        },
        complete: function () {
            $('.cards').hover(function (callback) {
                $(this).css("background-color", "#F2F2f2");
            }, function () {
                $(this).css("background-color", "#6E96D3");
            });
            callback();
        }
    });
});
