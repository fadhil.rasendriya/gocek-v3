from django.test import TestCase, Client
from django.urls import resolve, reverse
from .apps import PasarZaldyConfig
from .models import Pasar,Penjual
import datetime, tempfile
from .forms import PasarForms, PenjualForms
from django.urls import reverse

# Create your tests here.

class TestApp(TestCase):
	def test_app(self):
		self.assertEqual(PasarZaldyConfig.name, "pasar_zaldy")

class TestFormIsValid(TestCase):
    def test_user_already_exists(self):
        data = {
            'namaPasar': 'testclient',
            'jambukaPasar': '08:00',
            'jamtutupPasar': '08:30',
            'hotlinePasar': '3123131',
            'fotoPasar': 'test123',
            'alamatPasar': 'test123',
            'statuscovidPasar': 'Zona Kuning',
        }
        fors = PasarForms(data)
        self.assertTrue(fors.is_valid())
        self.assertTrue(fors.save)


class PasarMainTest(TestCase):
    def registerDummy(self):
        data = {'username': 'toru', 'password1': 'notchill', 'password2': 'notchill', 'email': 'toru@tkmail.com'}
        self.client.post('/account/signup/', data=data)

    def dummy_pasar(self):
        obj = Pasar.objects.create(
            namaPasar= 'testclient',
            jambukaPasar= '08:00',
            jamtutupPasar= '08:30',
            hotlinePasar= '3123131',
            fotoPasar= 'test123',
            alamatPasar= 'test123',
            statuscovidPasar= 'Zona Kuning')
        return obj

    def dummy_penjual(self):
        nama = "Penjual"
        kontak = "3423423"
        jenis = "APAKEK"
        pasar = self.dummy_pasar()
        obj = Penjual.objects.create(namaPenjual=nama, kontakPenjual=kontak, jenisJualan=jenis, pasarPenjual=pasar)
        return obj

    
    def test_add_pasar_API_success(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        self.client.post('/account/login/', data=data)
        a = 'Budi'
        b = '08:00'
        c = '08:30'
        d = '3123131'
        e = 'test123'
        f = 'test123'
        g = 'Zona Kuning'
        data = {'namaPasar': a, 'jambukaPasar': b,
                'jamtutupPasar': c, 'hotlinePasar': d, 'fotoPasar': e, 'alamatPasar': f, 'statuscovidPasar': g}
        response = self.client.post('/pasar/tambah/', data=data)
        json_response = eval(response.content.decode('utf8'))
        self.assertIn('berhasil', json_response['message'])


    def test_model_pasar_can_create(self):
        test = 'Anon'
        test_time_open = datetime.time(0, 0, 0)
        test_time_close = datetime.time(1, 0, 0)
        Pasar.objects.create(
            namaPasar= 'testclient',
            jambukaPasar= '08:00',
            jamtutupPasar= '08:00',
            hotlinePasar= '3123131',
            fotoPasar= 'test123',
            alamatPasar= 'test123',
            statuscovidPasar= 'Zona Kuning')
        object_counter = Pasar.objects.all().count()
        self.assertEqual(object_counter, 1)

    def test_add_pasar_success(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        self.client.post('/account/login/', data=data)
        TORU = 'TORU'
        data = {'name': TORU, 'alamat': TORU}
        response = self.client.post('/pasar/tambah/', data=data, follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn('Nama Pasar', html_response)

    def test_update_pasar_success(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        self.client.post('/account/login/', data=data)
        self.dummy_pasar()
        a = 'Budi'
        b = '08:00'
        c = '08:30'
        d = '3123131'
        e = 'test123'
        f = 'test123'
        g = 'Zona Kuning'
        data = {'namaPasar': a, 'jambukaPasar': b,
                'jamtutupPasar': c, 'hotlinePasar': d, 'fotoPasar': e, 'alamatPasar': f, 'statuscovidPasar': g}
        response = self.client.post('/pasar/1/updatepasar', data=data, follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn('Nama Pasar', html_response)


    def test_update_pasar_post_login(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        self.client.post('/account/login/', data=data)
        self.dummy_pasar()
        response = self.client.get('/pasar/1/updatepasar')
        self.assertNotEqual(response.status_code, 404)

    def test_get_pasar_API(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        self.client.post('/account/login/', data=data)
        self.dummy_pasar()
        response = self.client.get('/pasar/getpasarlist/')
        json_response = eval(response.content.decode('utf8'))
        self.assertNotEqual(json_response['items'][0].get('nama'), '')


    def test_penjual_API(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        self.client.post('/account/login/', data=data)
        self.dummy_penjual()
        data = {'id': '1'}
        response = self.client.get('/pasar/penjualapi/', data=data)
        json_response = eval(response.content.decode('utf8'))
        self.assertNotEqual(json_response['items'][0].get('nama'), '')


    def test_add_penjual_success(self):
            self.dummy_pasar()
            self.registerDummy()
            data = {'username': 'toru', 'password': 'notchill'}
            self.client.post('/account/login/', data=data)
            a = 'Budi'
            b = '08888888'
            c = 'Makanan'
            d = self.dummy_pasar
            data = {'namaPenjual': a, 'kontakPenjual': b, 'jenisJualan': c, 'pasarPenjual':d}
            response = self.client.post('/pasar/1/tambahpenjual', data=data, follow=True)
            html_response = response.content.decode('utf8')
            self.assertIn('Nama Penjual', html_response)


class TestURLRouting(TestCase):
    def test_mainpage_url_is_exist(self):
        response = Client().get('/pasar/')
        self.assertEqual(response.status_code, 200)

    def test_tambah_url_is_exist(self):
        response = Client().get('/pasar/tambah/')
        self.assertNotEqual(response.status_code, 404)

    def test_detail_url_is_exist(self):
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        activity = Pasar.objects.create(namaPasar="Pasar Senin", hotlinePasar="09421421", 
        statuscovidPasar = 'Aman', alamatPasar = "dsadadasdadasd", jambukaPasar= datetime.time(0,0,0),
        jamtutupPasar = datetime.time(1,0,0), fotoPasar=image )
        yes = activity.id
        response = Client().get('/pasar/detail/' +str(yes)+ '/')
        self.assertEqual(response.status_code, 200)
    
    def test_update_url_is_exist(self):
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        activity = Pasar.objects.create(namaPasar="Pasar Senin", hotlinePasar="09421421", 
        statuscovidPasar = 'Aman', alamatPasar = "dsadadasdadasd", jambukaPasar= datetime.time(0,0,0),
        jamtutupPasar = datetime.time(1,0,0), fotoPasar=image )
        get = activity.id
        response = Client().get('/pasar/' +str(get)+ '/updatepasar/')
        self.assertNotEqual(response.status_code, 404)

    def test_mainpage_using_template(self):
        response = Client().get('/pasar/')
        self.assertTemplateUsed(response, 'pasar_mainpage.html')


    def test_tambahpenjual_using_template(self):
        image = tempfile.NamedTemporaryFile(suffix=".jpg").name
        activity = Pasar.objects.create(namaPasar="Pasar Senin", hotlinePasar="09421421", 
        statuscovidPasar = 'Aman', alamatPasar = "dsadadasdadasd", jambukaPasar= datetime.time(0,0,0),
        jamtutupPasar = datetime.time(1,0,0), fotoPasar=image )
        get = activity.id
        response = Client().get('/pasar/' +str(get)+ '/tambahpenjual/')
        self.assertNotEqual(response.status_code, 404)


class TestPageContent(TestCase):
    def test_mainpage_in_template(self):
        response = Client().get('/pasar/')
        konten_html = response.content.decode('utf8')
        self.assertIn('Pasar', konten_html)
        self.assertIn('GOCEK', konten_html)
    #    self.assertIn('Bagi Informasi', konten_html)

    def test_tambahpage_should_login(self):
        response = Client().get('/pasar/tambah/', follow=True)
        konten_html = response.content.decode('utf8')
        self.assertIn('username', konten_html)
        self.assertIn('login', konten_html)
    #    self.assertIn('Bagi Informasi', konten_html)


class TestSTRPasar(TestCase):
    def test_pasar_str(self):
        pas = Pasar.objects.create(namaPasar='margonda')
        self.assertEqual(str(pas), 'margonda')

    def test_penjual_str(self):
        pas = Penjual.objects.create(namaPenjual='anto')
        self.assertEqual(str(pas), 'anto')

