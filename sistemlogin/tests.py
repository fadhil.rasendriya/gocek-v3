from django.test import TestCase
from django.contrib.auth.models import User
from django.urls.base import resolve
from . import views
# Create your tests here.


class LoginTestCase(TestCase):

    def test_login_url_exists(self):
        response = self.client.get('/account/login/')
        self.assertEqual(response.status_code, 200)

    def test_signup_url_exists(self):
        response = self.client.get('/account/signup/')
        self.assertEqual(response.status_code, 200)

    def test_login_func(self):
        found = resolve('/account/login/')
        self.assertEqual(found.func, views.log_in)

    def test_signup_func(self):
        found = resolve('/account/signup/')
        self.assertEqual(found.func, views.sign_up)

    def test_signup_success(self):
        users = User.objects.all()
        original_count = users.count()
        data = {'username': 'toru', 'password1': 'notchill', 'password2': 'notchill', 'email': 'toru@tkmail.com'}
        response = self.client.post('/account/signup/', data=data)

        self.assertEqual(response.status_code, 302)
        count = User.objects.all().count()
        self.assertEqual(original_count + 1, count)

    def test_signup_fail(self):
        users = User.objects.all()
        original_count = users.count()
        data = {'username': 'toru', 'password1': 'notchill1', 'password2': 'notchill2', 'email': 'toru@tkmail.com'}
        response = self.client.post('/account/signup/', data=data)
        html_response = response.content.decode('utf8')
        self.assertIn('Invalid inputs', html_response)
        count = User.objects.all().count()
        self.assertEqual(original_count, count)


    def registerDummy(self):
        data = {'username': 'toru', 'password1': 'notchill', 'password2': 'notchill', 'email': 'toru@tkmail.com'}
        self.client.post('/account/signup/', data=data)

    def test_login_success(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        response = self.client.post('/account/login/', data=data)
        self.assertEqual(response.status_code, 302)

    def test_login_fail(self):
        self.registerDummy()
        data = {'username': 'higuchi', 'password': 'notchill'}
        response = self.client.post('/account/login/', data=data)
        html_response = response.content.decode('utf8')
        self.assertIn('Invalid username or password', html_response)

    def test_logout_url(self):
        response = self.client.get('/account/logout/')
        self.assertEqual(response.status_code, 302)

    def test_logout_func(self):
        found = resolve('/account/logout/')
        self.assertEqual(found.func, views.log_out)

    def test_logout_success(self):
        self.registerDummy()
        data = {'username': 'toru', 'password': 'notchill'}
        self.client.post('/account/login/', data=data)
        session_id = self.client.cookies.get('sessionid')
        self.client.get('/account/logout/')
        self.assertNotEqual(self.client.cookies.get('sessionid'), session_id)
