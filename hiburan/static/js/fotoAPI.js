$(document).ready(function () {
    $.ajax({
        method: 'GET',
        data: {
            'id': $('#hiburanId').val()
        },
        url: '/hiburan/fotoapi/',
        success: function (result) {
            let fotoRow = $('#fotoRow');
            fotoRow.empty();
            let length = result['total_items'];
            for (let i = 0; i < length; i++) {
                let item = result['items'][i];
                let link_foto = item['link_foto'];
                fotoRow.append(
                    `
                    <tr>
                    <td style="color: grey;">${link_foto}</td>
                    </tr>
                    `
                );
            }
        }
    })
});
