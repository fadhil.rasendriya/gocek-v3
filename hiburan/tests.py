from django.test import TestCase, Client
from django.urls import resolve, reverse
from .models import Hiburan, Fotos
from .apps import *
from .forms import *
from . import views

import datetime

class TestApp(TestCase):
	def test_app(self):
		self.assertEqual(HiburanConfig.name, "hiburan")

# Create your tests here.
class TestRoute(TestCase):
    # template rs obj
    def hibDummy(self):
        test = 'test'
        hib = Hiburan.objects.create(nama = test, status = test, budget = test, kontak = test, alamat = test, jam_buka = test, jam_tutup = test, foto = test)
        return hib

    def test_function_used_main(self):
        response = resolve('/hiburan/')
        self.assertEqual(response.func, views.index)

    def test_function_addTempat(self):
        response = resolve('/hiburan/addTempat/')
        self.assertEqual(response.func, views.addTempat)    

    def test_function_used_update(self):
        hib = self.hibDummy()
        response = resolve('/hiburan/' + str(hib.pk) + '/update/')
        self.assertEqual(response.func, views.update)

    def test_function_used_detail(self):
        hib = self.hibDummy()
        response = resolve('/hiburan/detail/' + str(hib.pk) + '/')
        self.assertEqual(response.func, views.detail)

    def test_function_used_addFoto(self):
        hib = self.hibDummy()
        response = resolve('/hiburan/' + str(hib.pk) + '/addFoto/')
        self.assertEqual(response.func, views.addFoto)
    
    # template register account
    def registerDummy(self):
        data = {'username': 'said', 'password1': 'nizammmmm', 'password2': 'nizammmmm', 'email': 'iniapasih@gmail.com'}
        self.client.post('/account/signup/', data=data)
    
    def test_template_used_addTempat(self):
        self.registerDummy()
        data = {'username': 'said', 'password': 'nizammmmm'}
        self.client.post('/account/login/', data=data)
        response = self.client.get('/hiburan/addTempat/')
        self.assertTemplateUsed(response, 'hiburan/addTempat.html')    
    
    def test_template_used_update(self):
        self.registerDummy()
        data = {'username': 'said', 'password': 'nizammmmm'}
        self.client.post('/account/login/', data=data)
        hib = self.hibDummy()
        response = self.client.get('/hiburan/'+str(hib.pk)+"/update/", follow=True)
        self.assertTemplateUsed(response, 'hiburan/update.html')
    
    
    def test_template_used_detail(self):
        self.registerDummy()
        data = {'username': 'said', 'password': 'nizammmmm'}
        self.client.post('/account/login/', data=data)
        hib = self.hibDummy()
        response = self.client.get('/hiburan/detail/'+str(hib.pk)+"/", follow=True)
        self.assertTemplateUsed(response, 'hiburan/detail.html')

    def test_template_used_addFoto(self):
        self.registerDummy()
        data = {'username': 'said', 'password': 'nizammmmm'}
        self.client.post('/account/login/', data=data)
        hib = self.hibDummy()
        response = self.client.get('/hiburan/'+str(hib.pk)+"/addFoto/", follow=True)
        self.assertTemplateUsed(response, 'hiburan/addFoto.html')


    def test_tambah_hib_before_login(self):
        response = self.client.get('/hiburan/addTempat/', follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn('username', html_response)
        self.assertIn('login', html_response)

    def test_update_hib_before_login(self):
        hib = self.hibDummy()
        response = self.client.get('/hiburan/'+str(hib.pk)+"/update/", follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn('username', html_response)
        self.assertIn('login', html_response)

    def test_tambah_dokter_before_login(self):
        hib = self.hibDummy()
        response = self.client.get('/hiburan/'+str(hib.pk)+"/addFoto/", follow=True)
        html_response = response.content.decode('utf8')
        self.assertIn('username', html_response)
        self.assertIn('login', html_response)

    def test_landing_hib_after_login(self):
        self.registerDummy()
        data = {'username': 'said', 'password': 'nizammmmm'}
        self.client.post('/account/login/', data=data)
        response = self.client.get('/hiburan/', follow=True)
        self.assertEqual(response.status_code, 200)
    
    def test_tambah_hib_after_login(self):
        self.registerDummy()
        data = {'username': 'said', 'password': 'nizammmmm'}
        self.client.post('/account/login/', data=data)
        response = self.client.get('/hiburan/addTempat/', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_update_after_login(self):
        self.registerDummy()
        data = {'username': 'said', 'password': 'nizammmmm'}
        self.client.post('/account/login/', data=data)
        hib = self.hibDummy()
        response = self.client.get('/hiburan/'+str(hib.pk)+"/update/", follow=True)
        self.assertEqual(response.status_code, 200)

    def test_detail_after_login(self):
        self.registerDummy()
        data = {'username': 'said', 'password': 'nizammmmm'}
        self.client.post('/account/login/', data=data)
        hib = self.hibDummy()
        response = self.client.get('/hiburan/detail/'+str(hib.pk)+"/", follow=True)
        self.assertEqual(response.status_code, 200)

    def test_addFoto_after_login(self):
        self.registerDummy()
        data = {'username': 'said', 'password': 'nizammmmm'}
        self.client.post('/account/login/', data=data)
        hib = self.hibDummy()
        response = self.client.get('/hiburan/'+str(hib.pk)+"/addFoto/", follow=True)
        self.assertEqual(response.status_code, 200)


class TestActivity(TestCase):
    def test_model_hiburan_can_create(self):
        test = 'test'
        Hiburan.objects.create(nama = test, status = test, budget = test, kontak = test, alamat = test, jam_buka = test, jam_tutup = test, foto = test)
        counter = Hiburan.objects.all().count()
        self.assertEqual(counter, 1)
    

    
