var i = 1
$("#ubah-tema").click(function(){
    if (i) {
        i = 0
        $(".base-kategori").css("background-color", "#404040");
        $("body").css("background-color", "#282828");
        $(".tema").css("background-color", "#282828");
        $(".footer").css("background-color", "282828");
    } else {
        i = 1
        $(".base-kategori").css("background-color", "rgba(196,196,196,0.4)");
        $("body").css("background-color", "white");
        $(".tema").css("background-color", "white");
        $(".footer").css("background-color", "rgba(196,196,196,0.4)");
        
    }
})

$("#id_nama_rs").keyup(function( event ) {
    var q = $(this).val().length;
    $("#nama").text(q);
    if (q > 50) {
        $("#letter-count-nama").css("color", "red")
    } else {
        $("#letter-count-nama").css("color", "black")
    }
})

$("#id_link_foto").keyup(function( event ) {
    var q = $(this).val().length;
    $("#link-foto").text(q);
    if (q > 200) {
        $("#letter-count-link-foto").css("color", "red")
    } else {
        $("#letter-count-link-foto").css("color", "black")
    }
})

$("#id_link_rs").keyup(function( event ) {
    var q = $(this).val().length;
    $("#link-rs").text(q);
    if (q > 100) {
        $("#letter-count-link-rs").css("color", "red")
    } else {
        $("#letter-count-link-rs").css("color", "black")
    }
})

$("#id_alamat").keyup(function( event ) {
    var q = $(this).val().length;
    $("#alamat").text(q);
    if (q > 100) {
        $("#letter-count-alamat").css("color", "red")
    } else {
        $("#letter-count-alamat").css("color", "black")
    }
})

$("#id_nama_rs").keydown(function( event ) {
    var q = $(this).val().length;
    $("#nama").text(q);
    if (q > 50) {
        $("#letter-count-nama").css("color", "red")
    } else {
        $("#letter-count-nama").css("color", "black")
    }
})

$("#id_link_foto").keydown(function( event ) {
    var q = $(this).val().length;
    $("#link-foto").text(q);
    if (q > 200) {
        $("#letter-count-link-foto").css("color", "red")
    } else {
        $("#letter-count-link-foto").css("color", "black")
    }
})

$("#id_link_rs").keydown(function( event ) {
    var q = $(this).val().length;
    $("#link-rs").text(q);
    if (q > 100) {
        $("#letter-count-link-rs").css("color", "red")
    } else {
        $("#letter-count-link-rs").css("color", "black")
    }
})

$("#id_alamat").keydown(function( event ) {
    var q = $(this).val().length;
    $("#alamat").text(q);
    if (q > 100) {
        $("#letter-count-alamat").css("color", "red")
    } else {
        $("#letter-count-alamat").css("color", "black")
    }
})
