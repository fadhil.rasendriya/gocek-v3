from django.shortcuts import render

# Create your views here.

def index(request):
    return render(request, 'helloworld/index.html')

def cek_tempat(request):
    return render(request, 'helloworld/cek_tempat.html')

def bagi_informasi(request):
    return render(request, 'helloworld/bagi_informasi.html')
